<?php

/**
 * @file bt_upload.admin.inc
 *   Provides the admin form for the bt_upload
 */

/**
 * Admin page for setting variables for the upload module
 *
 * @return The form object
 */
function bt_upload_admin_settings() {
  $form = array();
  
  $form['bt_upload_parse'] = array(
    '#type' => 'radios',
    '#title' => t('Enable Thresholds'),
    '#description' => t(''),
    '#options' => array(
      t('Off'),
      t('On'),
    ),
    '#default_value' => 0,
  );
  $form['bt_upload_threshold_form'] = array(
    '#type' => 'fieldset',
    '#title' => t('Thresholds'),
    '#description' => t('Thresholds will automatically produce torrents for files that are uploaded beyond a current size.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['bt_upload_threshold_form']['bt_thresholds'] = array(
    '#type' => 'radios',
    '#title' => t('Enable Thresholds'),
    '#description' => t(''),
    '#options' => array(
      t('Off'),
      t('On'),
    ),
    '#default_value' => 0,
  );
  $form['bt_upload_threshold_form']['bt_threshold_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Threshold Limit'),
    '#description' => t('The size when a torrent will automatically be generated. Example: 1M = 1 MegaByte or 1K = 1 KiloByte.'),
    '#default_value' => '1M',
  );
  
  return system_settings_form($form);
}