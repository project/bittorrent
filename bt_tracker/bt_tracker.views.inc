<?php

/*
Views integration currently
 - support: Fields, Joins and Relationships
 - not support: Filters and Arguments
*/

/**
 * Describes table structure to Views 2
 * 
 * @return An array containing the table structures and relationships
 */
function bt_tracker_views_data() {
  $data = array(
    'bt_tracker_users' => array(
      'table' => array(
        'group' => t('BitTorrent - Tracker Users'),
        'base' => array (
          'field' => 'uid',
          'title' => t('Tracker User'),
          'help' => t('Users who are currently using the tracker.'),
        ),
        'join' => array(
          'users' => array(
            'left_field' => 'uid',
            'field' => 'uid',
          ),
        ),
      ),
      'uid' => array(
        'title' => t('Uid'),
        'help' => t('The User ID of the user.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'relationship' => array(
          'base' => 'users',
          'field' => 'uid',
          'handler' => 'views_handler_relationship',
          'label' => t('Users'),
        ),
      ),
      'passkey' => array(
        'title' => t('Passkey'),
        'help' => t('The unique Passkey of the user.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
      'passkey_status' => array(
        'title' => t('Passkey Status'),
        'help' => t('The status of the user\'s Passkey.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
      'bytes_uploaded' => array(
        'title' => t('Uploaded'),
        'help' => t('The Number of bytes the user has uploaded.'),
        'field' => array(
          'handler' => 'views_handler_field_file_size', //'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
      'bytes_downloaded' => array(
        'title' => t('Downloaded'),
        'help' => t('The number of bytes the user has downloaded.'),
        'field' => array(
          'handler' => 'views_handler_field_file_size', //'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
      'last_activity' => array(
        'title' => t('Last activity'),
        'help' => t('Last user successful BitTorrent tracker activity time.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
    ),
    'bt_tracker_connections' => array(
      'table' => array(
        'group' => t('BitTorrent - Tracker Connections'),
        'base' => array (
          'field' => 'uid',
          'title' => t('Tracker Connection'),
          'help' => t('The currently active connections to the tracker.'),
        ),
        'join' => array(
          'users' => array(
            'left_field' => 'uid',
            'field' => 'uid',
          ),
          'bt_torrents' => array(
            'left_field' => 'fid',
            'field' => 'fid',
          ),
          'files' => array(
            'left_field' => 'fid',
            'field' => 'fid',
          ),
        ),
      ),
      'uid' => array(
        'title' => t('UID'),
        'help' => t('The User ID of the user.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'relationship' => array(
          'base' => 'users',
          'field' => 'uid',
          'handler' => 'views_handler_relationship',
          'label' => t('Users'),
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_numeric',
        ),
      ),
      'peer_id' => array(
        'title' => t('Peer Id'),
        'help' => t('Unique ID generated by the client to identify itself in case the IP changes.'),
        'field' => array(
          'handler' => 'views_handler_field_php',
          'click sortable' => TRUE,
        ),
      ),
      'peer_key' => array(
        'title' => t('Peer Key'),
        'help' => t('The unique torrent client generated key.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
      'ip' => array(
        'title' => t('IP'),
        'help' => t('The IP the user is listening on.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
      'port' => array(
        'title' => t('Port'),
        'help' => t('The port the user is listening on for the handshake.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
      'fid' => array(
        'title' => t('File ID'),
        'help' => t('The Torrent-File ID for this session.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'relationship' => array(
          'base' => 'bt_torrents', //'files',
          'field' => 'fid',
          'handler' => 'views_handler_relationship',
          'label' => t('Torrents'), //t('Files'),
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_numeric',
        ),
      ),
      'bytes_uploaded' => array(
        'title' => t('Bytes Uploaded'),
        'help' => t('The number of bytes the user has uploaded this session.'),
        'field' => array(
          'handler' => 'views_handler_field_file_size', //'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
      'bytes_downloaded' => array(
        'title' => t('Bytes Downloaded'),
        'help' => t('The number of bytes the user has downloaded this session.'),
        'field' => array(
          'handler' => 'views_handler_field_file_size', //'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
      'bytes_left' => array(
        'title' => t('Bytes Left'),
        'help' => t('The number of bytes the user has left to download this session.'),
        'field' => array(
          'handler' => 'views_handler_field_file_size', //'views_handler_field',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ),
      ),
      'upload_speed' => array(
        'title' => t('Upload Speed'),
        'help' => t('The current user\'s upload speed this session (of this torrent).'),
        'field' => array(
          'handler' => 'views_handler_field_php', //'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
      'download_speed' => array(
        'title' => t('Download Speed'),
        'help' => t('The current user\'s download speed this session (of this torrent).'),
        'field' => array(
          'handler' => 'views_handler_field_php', //'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
      'last_announce' => array(
        'title' => t('Last Announce'),
        'help' => t('The last time the user contacted the tracker this session.'),
        'field' => array(
          'handler' => 'views_handler_field_date', //'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
    ),
  );
  
  return $data;
}
  