<?php

/**
 * @file bt_tracker.admin.inc
 *   Provide the admin interface for the bittorrent tracker module
 */

/**
 * Define BitTorrent Tracker settings
 *
 * @return The administrative form
 */
function bt_tracker_admin_settings() {
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General Tracker Settings'),
    '#description' => t('These are general settings for the BitTorrent Tracker. Mostly naming your tracker and setting the privacy scope.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['general']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Tracker Name'),
    '#description' => t('This is the name of the BitTorrent Tracker.'),
  );
  $form['general']['enabled'] = array(
    '#type' => 'radios',
    '#title' => t('Tracker Status'),
    '#options' => array(
      t('Offline'),
      t('Online'),
    ),
    '#description' => t('Determines whether the tracker is online of off.'),
  );
  $form['general']['scope'] = array(
    '#type' => 'radios',
    '#title' => t('Tracker Scope'),
    '#options' => array(
      t('Public'),
      t('Mixed'),
      t('Private'),
    ),
    '#description' => t('Sets the scope of the tracker. Public allows for the best performance and complete anonymity, but at the cost of statistics. Mixed tracks statistics of registered users and still allows anonymous access. Private restricts access to registered users and has the most fine grained control.'),
  );

  $form['intervals'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tracker Intervals'),
    '#description' => t('These settings control how often certain events occur within the tracker. This includes the announce and scrape intervals for the client as well as how often the tracker should auto-prune the active users information. Please note that how often the code is executed depends on when your cron jobs run'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['intervals']['announce_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Announce Interval'),
    '#description' => t('This is how often the torrent clients perform announce requests to the tracker.'),
  );
  $form['intervals']['scrape_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Scrape Interval'),
    '#description' => t('This is how often the torrent clients perform scrape requests to the tracker.'),
  );
  $form['intervals']['prune_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Auto-prune Interval'),
    '#description' => t('Determines when entries in the active users table will be automatically removed. Please note that this is also dependent on how often cron is run.'),
  );
  $form['intervals']['stats_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Statistics Update Interval'),
    '#description' => t('Determines how often the statistics for each torrent are regenerated based off of the active users table.'),
  );

  $form['limits'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tracker use requirements'),
    '#description' => t('These settings determine whether or not a user retrieves a result from the tracker.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['limits']['maximum_simultaneous_downloads'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum number of simultaneous downloads'),
    '#description' => t('This is how many downloads a user may have running simultaneously (0 for unlimited).'),
  );
  $form['limits']['minimum_ratio'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum sharing ratio'),
    '#description' => t('This is the minimum ratio a user must have to recieve a response from the tracker (0 for unlimited).'),
  );

  $view_list = views_get_all_views();
  
  $torrent_connections_view_option_list = array();
  foreach ($view_list as $view) {
    if (!$view->disabled) {
      $torrent_connections_view_option_list[$view->name] = $view->name; 
    }
  }  
  
  $form['interface'] = array(
    '#type' => 'fieldset',
    '#title' => t('Interface'),
    //'#description' => t(''),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['interface']['torrent_all_connections_view_name'] = array(
    '#type' => 'select',
    '#title' => t('Torrent all connections view name'),
    '#options' => $torrent_connections_view_option_list,
    '#description' => t('Selected view should accept following arguments: fid.'),
  );
  
  $form['path'] = array(
    '#type' => 'fieldset',
    '#title' => t('Path'),
    '#description' => t('After changing these options all users should redownload all their currently active torrent-files, because announce (and scrape) URL of the tracker will be changed. Or you can use Apache module "mod_rewrite" to perform aliasing from old announce (and scrape) URL to new, so both will be available and new will be used for currently not downloaded torrent-files and old for already downloaded torrent-files. Also after change these options do not forget to @clear_cache at !clear_cache_link.', array('@clear_cache' => t('Clear cached data'), '!clear_cache_link' => '<a href="/admin/settings/performance">admin/settings/performance</a>')),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['path']['use_announce_path'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use announce path'),
    '#description' => t('If enabled "Announce path" will be used as announce URL (ex: "http://example.com/announce"). If disabled "announce.php" and "scrape.php" files should be copied (or moved) to root Drupal directory and announce URL will be something like "http://example.com/announce.php". Using announce path can be bit slower, because of full Drupal loading, rather than bootstrap loading.'),
  );
  $form['path']['announce_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Announce path'),
    '#description' => t('Announce path should contain one "announce" substring, because it will be replaced with "scrape", to compose scrape URL! Ex: "bt/announce".'),
  );
  
  $form['announcing'] = array(
    '#type' => 'fieldset',
    '#title' => t('Announcing'),
    //'#description' => t(''),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['announcing']['send_peers_as_binary'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send peers as binary'),
    '#description' => t('Send peers as binary. Reduce output traffic. If sending peers contain any IPv6 peer, this option is ignored and peers send as dictionary (not as binary).'),
  );
  $form['announcing']['maximum_numwant'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum "numwant" value'),
    '#description' => t('Limit "numwant" value to this value. Or use as default if "numwant" is ommited. !spec', array('!spec' => '<a href="http://wiki.theory.org/BitTorrentSpecification#Tracker_Request_Parameters">Specification</a>')),
  );
  $form['announcing']['one_passkey_for_one_ip_only_at_same_time'] = array(
    '#type' => 'checkbox',
    '#title' => t('One passkey for one IP only at the same time'),
    '#description' => t('Reset user passkey if detect usage of one passkey by two or more IPs simultaneously. To protect users from passkey steal.'),
  );
  $form['announcing']['workaround'] = array(
    '#type' => 'fieldset',
    '#title' => t('Workarounds'),
    '#description' => t('Controls what workarounds use.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['announcing']['workaround']['workaround_set_started_if_connection_not_exists'] = array(
    '#type' => 'checkbox',
    '#title' => t('Set "started" if connection exists'),
    '#description' => t('When a new tracker added to already started torrent in a client. The client does not send "started" event for new tracker. [uTorrent 1.8.2; maybe others]'),
  );
  $form['announcing']['workaround']['workaround_unset_started_if_connection_exists'] = array(
    '#type' => 'checkbox',
    '#title' => t('Unset "started" if connection not exists'),
    '#description' => t('In some cases client can not send "stopped" event (before next "started" event), so we need to prevent a new start. [All clients]'),
  );
  $form['announcing']['workaround']['workaround_replace_questions_with_amps_in_query_string'] = array(
    '#type' => 'checkbox',
    '#title' => t('Replace question marks with ampersands in a query string'),
    '#description' => t('Some clients consider the announce URL as a final script URL, and does not recognize the passkey variable in it. [BitTorrent 4.2.2 @ CentOS/RHEL-4.x; BitTorrent 4.4.0 @ RHEL-5.3] !issue', array('!issue' => '<a href="http://drupal.org/node/508606">Issue #508606</a>')),
  );

  return system_settings_form(settings_prepare_settings_form($form));
}