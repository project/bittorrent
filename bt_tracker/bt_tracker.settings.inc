<?php

/**
 * Implementation of hook_settings_get_settings_info().
 * 
 * @see module "settings" ( http://drupal.org/project/settings )
 */
function bt_tracker_settings_get_settings_info()
{
  return array(
    'defaults' => array(
      // Settings
      'name' => variable_get('site_name', t('BitTorrent Tracker')),
      'enabled' => 1,
      'scope' => 0,
  
      'announce_interval' => 1800,
      'scrape_interval' => 900,
      'prune_interval' => 3600,
      'stats_interval' => 3600,
  
      'maximum_simultaneous_downloads' => 0,
      'minimum_ratio' => 0,
  
      'send_peers_as_binary' => TRUE,
  
      'workaround_set_started_if_connection_not_exists' => TRUE,
      'workaround_unset_started_if_connection_exists' => TRUE,
      'workaround_replace_questions_with_amps_in_query_string' => TRUE,

      'maximum_numwant' => 30,

      'use_announce_path' => TRUE,
      'announce_path' => 'announce',

      'one_passkey_for_one_ip_only_at_same_time' => TRUE,
  
      'torrent_all_connections_view_name' => 'bt_torrent_all_connections',
    
      // Internal Settings
      'announce_min_interval' => 600,
      'scrape_scope' => 0,
  
      // Variables
      'last_prune' => 0,
    ),
  );
}
