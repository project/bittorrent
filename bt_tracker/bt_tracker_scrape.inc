<?php

/**
 * The scrape portion of the tracker module.
 *
 * Accepts the input from the users torrent client
 * and acts accordingly.
 */
function bt_tracker_scrape() {
  // Do not continue parsing if the tracker is offline and let the client know that the tracker is offline.
  if (settings_get_for('bt_tracker', 'enabled') != 1) {
    bt_error('Tracker is Offline');
  }

  $response = array('files' => array());
  $hashes = array();

  $request_variables = split('&', $_SERVER['QUERY_STRING']);
  foreach ($request_variables as &$request_var) {
    preg_match('/info_hash=/', $request_var, $matches);

    if (count($matches) == 0 ) {
      unset($request_var);
    }
    else {
      // Strip 'info_hash=' from the begining of the value and urldecode it.
      $hashes[] = bin2hex(urldecode(substr($request_var, 10)));
    }
  }

  if (count($hashes[0]) == 0) {
    bt_error('Please supply an info_hash');
  }

  foreach ($hashes as $info_hash) {
    $valid = db_result(db_query("SELECT fid FROM {bt_torrents} WHERE info_hash = '%s'", $info_hash));
    if ($valid) {
      $response['files'][$info_hash] = bt_tracker_get_scrape_info($info_hash);
    }
  }

  if (count($response['files']) == 0) {
    bt_error('Invalid info hash(s)');
  }

  $response['flags'] = array();
  $response['flags']['min_request_interval'] = settings_get_for('bt_tracker', 'scrape_interval');

  // Loop through the files array updating info_hash keys into their correct binary strings
  $files = $response['files'];
  unset($response['files']);
  $response['files'] = array();
  foreach ($files as $info_hash => $scrape_info) {
    $response['files'][pack('H*', $info_hash)] = $scrape_info;
  }

  // Return the response
  bencode_response_raw(bencode($response));
}
