<?php

/**
 * @file bt_webseeding.admin.inc
 *   Provide the admin interface for the bittorrent webseeding module
 */

/**
 * Define BitTorrent WebSeeding settings
 *
 * @return The administrative form
 */
function bt_webseeding_admin_settings() {
  $form['info'] = array(
    '#type' => 'markup',
    '#value' => '<div class="description">'.t('WebSeeding allows the web server to act as a torrent client providing pieces of a download should no seeds be available. There are two implementation for WebSeeding. The !get_right implementation is very simple to use, the torrent client queries the server for pieces of the file. A caveat is that there is no limiting how often this is used and who may use it. The second implementation was proposed by !bit_tornado and it allows finer control over who may access files and when they may do so.', array('!get_right' => '<a href="http://www.getright.com/seedtorrent.html">GetRight</a>', '!bit_tornado' => '<a href="http://bittornado.com/docs/webseed-spec.txt">BitTornado</a>')).'</div>',
  );
  $form['bittornado']['webseeding_files_path'] = array(
    '#type' => 'textfield',
    '#title' => t('WebSeeding path'),
    '#description' => t('Path on server (relative) where torrent content files are stored.'),
  );
  $form['bittornado'] = array(
    '#type' => 'fieldset',
    '#title' => t('BitTornado'),
    '#description' => t('The BitTornado method is more secure, but requires more CPU usage as all data is passed through PHP.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['bittornado']['webseeding_bittornado'] = array(
    '#type' => 'checkbox',
    '#title'  => t('Enable BitTornado WebSeeding'),
    '#description' => t('This determines whether or not the tracker will support WebSeeding - BitTornado method.'),
  );
  $form['bittornado']['webseeding_bittornado_use_path'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use WebSeeding path'),
    '#description' => t('If enabled "WebSeeding path" will be used as WebSeeding URL (ex: "http://example.com/webseeding"). If disabled "webseeding.php" file should be copied (or moved) to root Drupal directory and webseeding URL will be something like "http://example.com/webseeding.php". Using webseeding path can be bit slower, because of full Drupal loading, rather than bootstrap loading.'),
  );
  $form['bittornado']['webseeding_bittornado_path'] = array(
    '#type' => 'textfield',
    '#title' => t('WebSeeding path'),
    '#description' => t('Ex: "bt/webseeding".'),
  );
  $form['getright'] = array(
    '#type' => 'fieldset',
    '#title' => t('GetRight'),
    '#description' => t('The GetRight method is less secure and may be abused.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['getright']['webseeding_getright'] = array(
    '#type' => 'checkbox',
    '#title'  => t('Enable GetRight WebSeeding'),
    '#description' => t('This determines whether or not the tracker will support WebSeeding - GetRight method.'),
  );
  $form['getright']['workaround'] = array(
    '#type' => 'fieldset',
    '#title' => t('Workarounds'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['getright']['workaround']['workaround_getright_append_torrent_name_to_file_url'] = array(
    '#type' => 'checkbox',
    '#title'  => t('Append torrent "name" to file URL'),
    '#description' => t('uTorrent does not support torrents with webseeding URLs ending with a slash ("/").'),
  );
  
  return system_settings_form(settings_prepare_settings_form($form));
}