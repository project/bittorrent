<?php

/**
 * Returns a message to the BitTorrent client trying to request the piece
 * 
 * @param $status_code
 *   The numerical status code to return.
 * @param $message
 *   The message to return along with the status code.
 * @param $exit
 *   Determines whether or not processing should stop.
 */
function seed_message($status_code = 503, $message = '' , $exit = false) {
  ob_clean();
  if ($status_code == 503) {
    $header = '503 Service Temporarily Unavailable';
  }
  else if ($status_code == 400) {
    $header = '400 Bad Request';
  }
  else if ($status_code == 500) {
    $header = '500 Internal Server Error';
  }
  else if ($status_code == 403) {
    $header = '403 Forbidden';
  }
  else if ($status_code == 200) {
    $header = '200 Ok';
  }
  
  header('HTTP/1.0 '. $header);
  print($message);
  
  if ($exit) {
    exit();
  }
}

/**
 * The web-seeding portion of the tracker.
 *
 * Accepts the input from the users torrent client and acts accordingly.
 */
function bt_webseeding_seed() {
  if (!settings_get('webseeding_bittornado')) {
    seed_message(403, 'BitTornado Web-Seeding implementation is disabled on this tracker.', TRUE);
  }
  
  // Process the variables...
  $required_keys = array('info_hash', 'piece');
  $optional_keys = array('ranges');
  $request = array();
  
  foreach($required_keys as $key) {
    if(!array_key_exists($key, $_GET)) {
      seed_message(400, 'Missing Key: '. $key, TRUE);
    }
    else {
      $request[$key] = $_GET[$key];
    }
  }
  
  foreach($optional_keys as $key) {
    if(array_key_exists($key, $_GET)) {
      $request[$key] = $_GET[$key];
    }
  }
  
  /**
   * Lookup the info_hash and verify that we are indeed tracking it and it supports web seeding. Retrieve the torrent file should all criteria be met.
   */
  /*$result = db_result(db_query("SELECT web_seed FROM {bt_torrents} btt WHERE btt.info_hash = %b", $request['info_hash']));
  if (isset($result) || $result == 0) {
    seed_message(400, 'Tracker is currently not tracking that torrent or the torrent does not support web seeding.', TRUE);
  }*/
  
  // TODO: Add server control settings

  $torrent_data = db_fetch_array(db_query('SELECT fid, total_size FROM {bt_torrents} WHERE info_hash = "%s"', $request['info_hash']));
  if ($torrent_data === FALSE) {
    seed_message(400, 'Torrent does not exist, please consider uploading it.', TRUE);
  }
  
  $fid = $torrent_data['fid'];
  $total_size = $torrent_data['total_size'];
  $file = field_file_load($fid);
  $metadata = file_get_contents($file['filepath']);
  $torrent = bdecode($metadata);

  $piece_size = $torrent['info']['piece length'];
  
  // Verify that the piece exists within the torrent
  // Zero piece always exists, so don't check it
  if ($request['piece'] != 0) {
    $piece_count = $total_size / $piece_size;
    if (!is_numeric($request['piece']) || ($request['piece'] < 0) || ($request['piece'] > ceil($piece_count) - 1)) {
      seed_message(400, 'Invalid Piece requested: '.$request['piece'], TRUE);
    }
  }

  if (!isset($request['range'])) {
    $range_from = $piece * $piece_size;
    $range_to   = $range_from + $piece_size;
    $request['range'] = $range_from.'-'.$range_to;
  }
  
  // Translate the byte ranges and verify them
  $ranges = explode(',', $request['range']);
  $ranges = asort($ranges);
  foreach($ranges as &$range) {
    $range = explode('-', $range, 2);
  }
  foreach($ranges as $range) {
    if ($range[0] > $range[1] || $range[1] > $total_size) {
      seed_message(400, 'Invalid range requested', TRUE);
    }
  }

  // Build the base path
  $base_path = settings_get('webseeding_files_path').'/'.$fid.'/';
  
  // Compose file info list
  $files = array();
  if (!isset($torrent['info']['files'])) {
    // Single file torrent
    $files[] = array(
      0 => 0,
      1 => $file['length'],
      'path' => $torrent['info']['name'],
    );
  }
  else {
    // Multiple file torrent
    $offset = 0;
    foreach($torrent['info']['files'] as $file){
      $files[] = array(
        0 => $offset,
        1 => $offset + $file['length'],
        'path' => $torrent['info']['name'].'/'.join('/', $file['path']),
      );
      $offset += $file['length'];
    }
  }
  
  // Retrieve the data
  // I'm proud of this my code block ;) 'cause it's N complex, not N^2 like in prev ver [overall aka liquixis was here] 8b
  $file_index = 0;
  $range_index = 0;
  $data = '';
  while( ($file_index <= count($files) - 1) && ($range_index <= count($ranges - 1)) ) {
    $file = $files[$file_index];
    $range = $ranges[$range_index];
    
    if (($range[0] < $file[1]) && ($range[1] > $file[0])) {
      $data_from = max($range[0], $file[0]);
      $data_size = min($range[1], $file[1]) - $data_from;
      
      if (!isset($handle)) {
        $handle = fopen($base_path.$file['path'], 'rb');
      }
      
      fseek($handle, $data_from - $file[0]);
      $data .= fread($handle, $data_size);
    }

    // Go to next file
    if ($file[1] <= $range[1]) {
      $file_index++;
      
      if (isset($handle)) {
        fclose($handle);
        unset($handle);
      }
    }
    
    // Go to next range
    if ($range[1] <= $file[1]) {
      $tange_index++;
    }
  }
  
  seed_message(200, $data, TRUE);
}
