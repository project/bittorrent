<?php

/**
 * Implementation of hook_settings_get_settings_info().
 * 
 * @see module "settings" ( http://drupal.org/project/settings )
 */
function bt_webseeding_settings_get_settings_info()
{
  return array(
    'defaults' => array(
      // Settings
      'webseeding_bittornado' => TRUE,
      'webseeding_bittornado_use_path' => TRUE,
      'webseeding_bittornado_path' => 'webseeding',
      'webseeding_getright' => FALSE,
      'webseeding_files_path' => 'webseeding', // path inside "files" directory

      'workaround_getright_append_torrent_name_to_file_url' => TRUE,
    ),
  );
}
