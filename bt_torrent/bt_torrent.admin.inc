<?php

/**
 * @file bt_torrent.admin.inc
 *   Provides the admin form for the torrentfield node field type
 */

/**
 * Admin page for setting variables for the node field type
 *
 * @return The form object
 */
function bt_torrent_admin_settings() {
  $form['announce'] = array(
    '#type' => 'fieldset',
    '#title' => t('Announce URLs'),
    '#description' => t('Control where the torrents\' URLs will point.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['announce']['override_announce'] = array(
    '#type' => 'radios',
    '#title' => t('Override Announce URL'),
    '#options' => array(
      t('Leave Intact'),
      t('Append'),
      t('Override'),
    ),
    '#description' => t('This determines whether or not the announce URLs of the torrents being downloaded should be changed. Append will simply add your URL to the torrent\'s list of announce URLs, where Override will replace the current announce URL(s)'),
  );
  $form['announce']['override_announce_url'] = array(
    '#type' => 'textfield',
    '#title' => t('New Announce URL'),
    '#description' => t('This is the URL to use for the torrents being uploaded.')
  );

  $form['torrent_filename_template'] = array(
    '#type' => 'textfield',
    '#title' => t('Torrent filename template'),
    '#description' => t('If !token module is enabled, this will be used as template for torrent-file name, otherwise original torrent-file name will be used. Available tokens: fid, nid, vid, uid, user-name, type-name, /*type-title*/, field-name, /*field-title*/, delta, /*name*/, server-name, tracker-name, site-name, file-name, node-title. Unique field combintations: [fid], [vid, field-name, delta].', array('!token' => '<a href="http://drupal.org/project/token">Token</a>')),
  );
  
  $form['put_node_url_into_comment'] = array(
    '#type' => 'checkbox',
    '#title' => t('Put node URL into comment field of torrent file'),
    '#description' => t('If set, torrent node details page URL will be put into comment field of torrent file.'),
  );

  $form['remove_passkeys_from_uploaded_torrent_files'] = array(
    '#type' => 'select',
    '#title' => t('Remove passkeys from uploaded torrent files'),
    '#options' => array(
      0 => t('Never'),
      1 => t('At moment of torrent-file download'),
      2 => t('At moment of torrent-file upload'),
    ),
    '#description' => t('Automatically remove passkey values from announce URLs in uploaded torrent files. Changing this option does not affect existing torrent-files.'),
  );
  
  $form['cron'] = array(
    '#type' => 'fieldset',
    '#title' => t('CRON Jobs'),
    '#description' => t('Controls what CRON jobs run within this module.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['cron']['scrape_cron'] = array(
    '#type' => 'radios',
    '#title' => t('Scrape other trackers'),
    '#options' => array(
      t('Do not scrape'),
      t('Scrape for statistics'),
    ),
    '#description' => t('Use CURL to load the statistics for torrents that are not tracked locally.'),
  );
  $form['cron']['scrape_cron_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Scrape Interval'),
    '#description' => t('If enabled, how often the tracker should scrape other trackers.'),
  );
  $form['cron']['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced'),
    '#description' => t('Advanced settings for the cron run. Note that altering these may make the cron run take longer to run. Only alter these if you are certain that your php_execute_time is large enough to handle it.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 2,
  );
  $form['cron']['advanced']['scrape_cron_count'] = array(
    '#type' => 'textfield',
    '#title' => t('Scrapes per cron'),
    '#description' => t('How many trackers to scrape per cron run.'),
  );
  $form['cron']['advanced']['scrape_cron_timeout'] = array(
    '#type' => 'textfield',
    '#title' => t('Scrape timeout'),
    '#description' => t('How many seconds to wait for a tracker to be scraped. Make sure that "Scrapes per cron" multiplied by "Scrape timeout" is less than your php_execute_time.'),
  );
 
  return system_settings_form(settings_prepare_settings_form($form));
}