<?php

/*
Views integration currently
 - support: Fields, Joins and Relationships
 - not support: Filters and Arguments
*/

/**
 * Describes table structure to Views 2
 * 
 * @return An array containing the table structures and relationships
 */
function bt_torrent_views_data() {
  $data = array(
    'bt_torrents' => array(
      'table' => array(
        'group' => t('BitTorrent - Torrents'),
        'base' => array (
          'field' => 'fid',
          'title' => t('Torrent'),
          'help' => t('Torrents of BitTorrent module.'),
        ),
        'join' => array(
          'files' => array(
            'left_field' => 'fid',
            'field' => 'fid',
          ),
        ),
      ),
      'fid' => array(
        'title' => t('Fid'),
        'help' => t('The File ID of the File'),
        'field' => array(
          'handler' => 'views_handler_field', //'views_handler_field_node',
          'click sortable' => TRUE,
        ),
        'relationship' => array(
          'base' => 'files',
          'field' => 'fid',
          'handler' => 'views_handler_relationship',
          'label' => t('Files'),
        ),
        /*'argument' => array(
          'handler' => 'views_handler_argument_file_fid', //'views_handler_argument_numeric',
        ),*/
      ),
      'info_hash' => array(
        'title' => t('Information Hash'),
        'help' => t('The info_hash of the Torrent.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => FALSE,
        ),
      ),
      'announce_url' => array(
        'title' => t('Announce URL'),
        'help' => t('The URL the torrent will use to announce.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => FALSE,
        ),
      ),
      'total_size' => array(
        'title' => t('Total Size'),
        'help' => t('The total size of torrent content.'),
        'field' => array(
          'handler' => 'views_handler_field_file_size', //'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
      'snatches' => array(
        'title' => t('Snatches'),
        'help' => t('The number of times the torrent has been downloaded.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
      'seeders' => array(
        'title' => t('Seeders'),
        'help' => t('The number of users seeding the torrent.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
      'leechers' => array(
        'title' => t('Leechers'),
        'help' => t('The number of users leeching the torrent.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
      'downloaded' => array(
        'title' => t('Downloaded'),
        'help' => t('The number of times the torrent has been completely downloaded.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
      'last_seed' => array(
        'title' => t('Last Seed'),
        'help' => t('The last time any user seeded.'),
        'field' => array(
          'handler' => 'views_handler_field_date', //'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
    ),
    'bt_torrents_files' => array(
      'table' => array(
        'group' => t('BitTorrent - Torrent Files'),
        'base' => array (
          'field' => 'fid',
          'title' => t('Torrent File'),
          'help' => t('Info about files contained in Torrents.'),
        ),
        'join' => array(
          'files' => array(
            //'left_table' => 'bt_torrents', //NEW
            'left_field' => 'fid',
            'field' => 'fid',
          ),
        ),
      ),
      'fid' => array(
        'title' => t('Fid'),
        'help' => t('The File ID of the File.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'relationship' => array(
          'base' => 'files',
          'field' => 'fid',
          'handler' => 'views_handler_relationship',
          'label' => t('Files'),
        ),
      ),
      'length' => array(
        'title' => t('Length'),
        'help' => t('The size of the file in bytes.'),
        'field' => array(
          'handler' => 'views_handler_field_file_size', //'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
      'path' => array(
        'title' => t('Path'),
        'help' => t('The relative path of the file within the torrent.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
      'name' => array(
        'title' => t('Filename'),
        'help' => t('The name of the file.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
    ),
  );
  
  return $data;
}