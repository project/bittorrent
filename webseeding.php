<?php

// Require the bt functions
require_once('bt_common.inc');

// Include bootstrap.inc and run the bootstrap
include_once("includes/bootstrap.inc");
drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);

// Initializes $conf so we can use variable_get
$conf = variable_init(isset($conf) ? $conf : array());

// Include "bt_tracker" "bt_tracker_announce"
module_load_include('inc', 'bt_webseeding', 'bt_webseeding_seed');

bt_webseeding_seed();
