<?php

/**
 * @file bt_profile.admin.inc
 *   Provides the admin form for the torrentfield node field type
 */

/**
 * Admin page for setting variables for the node field type
 *
 * @return The form object
 */
function bt_profile_admin_settings() {
  $view_list = views_get_all_views();
  
  $user_torrents_view_option_list = array();
  foreach ($view_list as $view) {
    if (!$view->disabled) {
      $user_torrents_view_option_list[$view->name] = $view->name; 
    }
  }  
 
  $form['content_profile_node_type_name'] = array(
    '#type' => 'select',
    '#title' => t('Content profile node type name'),
    '#options' => module_exists('content_profile') ? content_profile_get_types('names') : array(),
    //'#description' => t(''),
  );
  $form['own_torrents_view_name'] = array(
    '#type' => 'select',
    '#title' => t('Own torrents view name'),
    '#options' => $user_torrents_view_option_list,
    '#description' => t('Selected view should accept following arguments: uid.'),
  );
  $form['seeding_torrents_view_name'] = array(
    '#type' => 'select',
    '#title' => t('Seeding torrents view name'),
    '#options' => $user_torrents_view_option_list,
    '#description' => t('Selected view should accept following arguments: uid.'),
  );
  $form['leeching_torrents_view_name'] = array(
    '#type' => 'select',
    '#title' => t('Leeching torrents view name'),
    '#options' => $user_torrents_view_option_list,
    '#description' => t('Selected view should accept following arguments: uid.'),
  );
  /*$form['bind_ip_to_passkey'] = array(
    '#type' => 'checkbox',
    '#title' => t('Bind IP to Passkey'),
    '#description' => t('Used just as default value if "Bind IP to Passkey option is user configurable" option is set.'),
  );
  $form['bind_ip_to_passkey_user_configurable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Bind IP to Passkey option is user configurable'),
    '#description' => t(''),
  );*/
  
  return system_settings_form(settings_prepare_settings_form($form));
}
