<?php

// Load the library containing all our awesome bittorrent code
require_once(drupal_get_path('module', 'bt_profile').'/../includes/bt_common.inc');

function drupal_ahah($output, $params = array())
{
  $javascript = drupal_add_js(null, null);

  unset($javascript['core']['misc/jquery.js']);
  //unset($javascript['core']['misc/drupal.js']);

  // Remove "DHTML Menu" bug.
  if( module_exists('dhtml_menu') )
    unset($javascript['module'][drupal_get_path('module', 'dhtml_menu').'/dhtml_menu.js']); //!!!

  $output_js = drupal_get_js('header', $javascript);
  drupal_json(array('status' => true, 'data' => drupal_get_css().$output_js.$output) + $params);
  exit;
}

/**
 * Implementation of hook_init().
 *
 * Load required includes and css files.
 */
function bt_profile_init() {
  drupal_add_css(dirname(drupal_get_path('module', 'bt_profile')).'/bittorrent_views.css');
}

/**
 * Implementation of hook_views_api().
 */
function bt_profile_views_api() {
  return array(
    'api' => 2,
    'path' => drupal_get_path('module', 'bt_profile'),
  );
}

/**
 * Implementation of hook_content_extra_fields().
 */
function bt_profile_content_extra_fields($node_type) {
  $extra_fields = array();

  // Workaround for update bug. See: http://drupal.org/node/516828
  if (function_exists('settings_get')) {
    if ($node_type == settings_get('content_profile_node_type_name')) {
      $extra_fields['bt_profile_passkey'] = array(
        'label' => t('Passkey'),
        'description' => t('Private User Passkey'),
        'weight' => 10,
      );
      $extra_fields['bt_profile_ip'] = array(
        'label' => t('IP'),
        'description' => t('Your current IP'),
        'weight' => 11,
      );
      $extra_fields['bt_profile_uploaded'] = array(
        'label' => t('Uploaded'),
        'description' => t('Bytes Uploaded'),
        'weight' => 12,
      );
      $extra_fields['bt_profile_downloaded'] = array(
        'label' => t('Downloaded'),
        'description' => t('Bytes Downloaded'),
        'weight' => 13,
      );
      $extra_fields['bt_profile_ratio'] = array(
        'label' => t('Ratio'),
        'description' => t('(Bytes Uploaded)/(Bytes Downloaded)'),
        'weight' => 14,
      );
      $extra_fields['bt_profile_last_activity'] = array(
        'label' => t('Last activity'),
        'description' => t('Timestamp of last announcing'),
        'weight' => 15,
      );
      $extra_fields['bt_profile_own_torrents'] = array(
        'label' => t('Own torrents'),
        'description' => t('Own torrents'),
        'weight' => 16,
      );
      $extra_fields['bt_profile_seeding_torrents'] = array(
        'label' => t('Seeding torrents'),
        'description' => t('Seeding torrents'),
        'weight' => 17,
      );
      $extra_fields['bt_profile_leeching_torrents'] = array(
        'label' => t('Leeching torrents'),
        'description' => t('Leeching torrents'),
        'weight' => 18,
      );
    }
  }
 
  return $extra_fields;
}

/**
 * Implementation of hook_nodeapi().
 */
function bt_profile_nodeapi(&$node, $op, $teaser, $page) {
  if ($node->type == settings_get('content_profile_node_type_name')) {
    if ($op == 'view') {
      global $user;
      
      $tracker_user = db_fetch_array(db_query('SELECT passkey, passkey_status, bytes_uploaded, bytes_downloaded, last_activity FROM {bt_tracker_users} WHERE uid = %d', $node->uid));
      
      // If the user is the owner of the profile.
      if ($user->uid == $node->uid) {
        $node->content['bt_profile_passkey'] = array(
          '#title' => t('Passkey'),
          '#type' => 'item',
          '#weight' => module_exists('content') ? content_extra_field_weight($node->type, 'bt_profile_passkey') : 10,
          '#value' => $tracker_user['passkey'].' ('.t('visible only for you').')',
        );
        $node->content['bt_profile_ip'] = array(
          '#title' => t('IP'),
          '#type' => 'item',
          '#weight' => module_exists('content') ? content_extra_field_weight($node->type, 'bt_profile_ip') : 11,
          '#value' => $_SERVER['REMOTE_ADDR'].' ('.t('visible only for you').')',
        );
      }

      $node->content['bt_profile_uploaded'] = array(
        '#title' => t('Uploaded'),
        '#type' => 'item',
        '#weight' => module_exists('content') ? content_extra_field_weight($node->type, 'bt_profile_passkey') : 12,
        '#value' => bt_format_size($tracker_user['bytes_uploaded']),
      );
      $node->content['bt_profile_downloaded'] = array(
        '#title' => t('Downloaded'),
        '#type' => 'item',
        '#weight' => module_exists('content') ? content_extra_field_weight($node->type, 'bt_profile_passkey') : 13,
        '#value' => bt_format_size($tracker_user['bytes_downloaded']),
      );
      $node->content['bt_profile_ratio'] = array(
        '#title' => t('Ratio'),
        '#type' => 'item',
        '#weight' => module_exists('content') ? content_extra_field_weight($node->type, 'bt_profile_passkey') : 14,
        '#value' => bt_format_ratio($tracker_user['bytes_uploaded'], $tracker_user['bytes_downloaded']),
      );
      $node->content['bt_profile_last_activity'] = array(
        '#title' => t('Last activity'),
        '#type' => 'item',
        '#weight' => module_exists('content') ? content_extra_field_weight($node->type, 'bt_profile_passkey') : 15,
        '#value' => format_date($tracker_user['last_activity']),
      );
      
      /*$node->content['bt_profile_own_torrents'] = array(
        '#title' => 'Own Torrents',
        '#type' => 'item',
        '#weight' => module_exists('content') ? content_extra_field_weight($node->type, 'bt_profile_own_torrents') : 16,
        '#value' => views_embed_view(settings_get('own_torrents_view_name'), 'default', $node->uid),
      );
      $node->content['bt_profile_seeding_torrents'] = array(
        '#title' => 'Seeding Torrents',
        '#type' => 'item',
        '#weight' => module_exists('content') ? content_extra_field_weight($node->type, 'bt_profile_seeding_torrents') : 17,
        '#value' => views_embed_view(settings_get('seeding_torrents_view_name'), 'default', $node->uid),
      );
      $node->content['bt_profile_leeching_torrents'] = array(
        '#title' => 'Leeching Torrents',
        '#type' => 'item',
        '#weight' => module_exists('content') ? content_extra_field_weight($node->type, 'bt_profile_leeching_torrents') : 18,
        '#value' => views_embed_view(settings_get('leeching_torrents_view_name'), 'default', $node->uid),
      );*/
      
      $node->content['bt_profile_own_torrents'] = form_expand_ahah(array(
        '#id' => 'user_own_torrents_button',
        '#attributes' => array('style' => 'margin: 2px;'),
        '#value' => t('Show Own Torrents'),
        '#type' => 'button',
        '#weight' => module_exists('content') ? content_extra_field_weight($node->type, 'bt_profile_own_torrents') : 16,
        '#ahah' => array(
          'path' => 'bittorrent/ahah/user_own_torrents/'.$node->uid,
          'wrapper' => 'user_own_torrents',
          'effect' => 'slide',
        ),
        '#suffix' => '<div id="user_own_torrents" style="clear: both;"></div>',
      ));
      $node->content['bt_profile_seeding_torrents'] = form_expand_ahah(array(
        '#id' => 'user_seeding_torrents_button',
        '#attributes' => array('style' => 'margin: 2px;'),
        '#value' => t('Show Seeding Torrents'),
        '#type' => 'button',
        '#weight' => module_exists('content') ? content_extra_field_weight($node->type, 'bt_profile_seeding_torrents') : 17,
        '#ahah' => array(
          'path' => 'bittorrent/ahah/user_seeding_torrents/'.$node->uid,
          'wrapper' => 'user_seeding_torrents',
          'effect' => 'slide',
        ),
        '#suffix' => '<div id="user_seeding_torrents" style="clear: both;"></div>',
      ));
      $node->content['bt_profile_leeching_torrents'] = form_expand_ahah(array(
        '#id' => 'user_leeching_torrents_button',
        '#attributes' => array('style' => 'margin: 2px;'),
        '#value' => t('Show Leeching Torrents'),
        '#type' => 'button',
        '#weight' => module_exists('content') ? content_extra_field_weight($node->type, 'bt_profile_leeching_torrents') : 18,
        '#ahah' => array(
          'path' => 'bittorrent/ahah/user_leeching_torrents/'.$node->uid,
          'wrapper' => 'user_leeching_torrents',
          'effect' => 'slide',
        ),
        '#suffix' => '<div id="user_leeching_torrents" style="clear: both;"></div>',
      ));
    }
  }
}

/**
 * Implementation of hook_form_alter().
 * 
 * Adds BitTorrent fields to Content Profile node edit forms.
 */
/*function bt_profile_form_alter(&$form, $form_state, $form_id) {
  if (isset($form['#node']) && $form['#node']->type .'_node_form' == $form_id) {
    // Note - doing this to make sure the delete checkbox stays in the form.
    $form['#cache'] = TRUE;

    $form['bt_profile'] = array(
      '#type' => 'fieldset',
      '#title' => t('bt_profile settings'),
      '#access' => user_access('administer bt_profile'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
      '#weight' => -2,
      //'#attributes' => array('class' => 'menu-item-form'),
    );
    $item = $form['#node']->menu;

    if ($item['mlid']) {
      // There is an existing link.
      $form['bt_profile']['delete'] = array(
        '#type' => 'checkbox',
        '#title' => t('Delete this menu item.'),
      );
    }
    if (!$item['link_title']) {
      $form['menu']['#collapsed'] = TRUE;
    }

    foreach (array('mlid', 'module', 'hidden', 'has_children', 'customized', 'options', 'expanded', 'hidden', 'parent_depth_limit') as $key) {
      $form['menu'][$key] = array('#type' => 'value', '#value' => $item[$key]);
    }
    $form['menu']['#item'] = $item;

    $form['menu']['link_title'] = array('#type' => 'textfield',
      '#title' => t('Menu link title'),
      '#default_value' => $item['link_title'],
      '#description' => t('The link text corresponding to this item that should appear in the menu. Leave blank if you do not wish to add this post to the menu.'),
      '#required' => FALSE,
    );
    // Generate a list of possible parents (not including this item or descendants).
    $options = menu_parent_options(menu_get_menus(), $item);
    $default = $item['menu_name'] .':'. $item['plid'];
    if (!isset($options[$default])) {
      $default = 'primary-links:0';
    }
    $form['menu']['parent'] = array(
      '#type' => 'select',
      '#title' => t('Parent item'),
      '#default_value' => $default,
      '#options' => $options,
      '#description' => t('The maximum depth for an item and all its children is fixed at !maxdepth. Some menu items may not be available as parents if selecting them would exceed this limit.', array('!maxdepth' => MENU_MAX_DEPTH)),
      '#attributes' => array('class' => 'menu-title-select'),
    );
    $form['#submit'][] = 'menu_node_form_submit';

    $form['menu']['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight'),
      '#delta' => 50,
      '#default_value' => $item['weight'],
      '#description' => t('Optional. In the menu, the heavier items will sink and the lighter items will be positioned nearer the top.'),
    );
  }
}*/

///////////////////////////////////////////////
///////////////////////////////////////////////
///////////////////////////////////////////////

/**
 * Implementation of hook_menu().
 */
function bt_profile_menu() {
  $items = array();

  $items['admin/bt/profile'] = array(
    'title' => 'BT Profile',
    'description' => 'Configure BitTorrent Profile settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('bt_profile_admin_settings'),
    'access arguments' => array('administer site configuration'),
    'file' => 'bt_profile.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  $items['bittorrent/ahah/user_own_torrents/%'] = array(
    'title' => t('Content Form'),
    'page callback' => 'bt_profile_ahah_user_own_torrents',
    'page arguments' => array(3),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  $items['bittorrent/ahah/user_seeding_torrents/%'] = array(
    'title' => t('Content Form'),
    'page callback' => 'bt_profile_ahah_user_seeding_torrents',
    'page arguments' => array(3),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  $items['bittorrent/ahah/user_leeching_torrents/%'] = array(
    'title' => t('Content Form'),
    'page callback' => 'bt_profile_ahah_user_leeching_torrents',
    'page arguments' => array(3),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  
  return $items;
}

/**
 * Implementation of hook_settings_get_settings_info().
 * 
 * @see module "settings" ( http://drupal.org/project/settings )
 */
function bt_profile_settings_get_settings_info()
{
  global $base_url;

  return array(
    'defaults' => array(
      // Settings
      'content_profile_node_type_name' => 'profile', //NULL,
      'own_torrents_view_name' => 'bt_user_own_torrents', //NULL,
      'seeding_torrents_view_name' => 'bt_user_seeding_torrents', //NULL,
      'leeching_torrents_view_name' => 'bt_user_leeching_torrents', //NULL,
      //'bind_ip_to_passkey' => 0, // 0 - never, 1 - always
      //'bind_ip_to_passkey_user_configurable' => 0, // 0 - no, 1 - yes
    ),
  );
}

function bt_profile_ahah_user_own_torrents($uid) {
  $output = views_embed_view(settings_get('own_torrents_view_name'), 'default', $uid);
  drupal_ahah($output);
}

function bt_profile_ahah_user_seeding_torrents($uid) {
  $output = views_embed_view(settings_get('seeding_torrents_view_name'), 'default', $uid);
  drupal_ahah($output);
}

function bt_profile_ahah_user_leeching_torrents($uid) {
  $output = views_embed_view(settings_get('leeching_torrents_view_name'), 'default', $uid);
  drupal_ahah($output);
}
