<?php

/**
 * Implementation of hook_views_default_views().
 */
function bt_profile_views_default_views() {
  $base_path = '/'.dirname(drupal_get_path('module', 'bt_profile')).'/images';
  
  $views = array();

  $view = new view;
  $view->name = 'bt_user_all_torrents';
  $view->description = '';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('relationships', array(
    'field_torrent_file_fid' => array(
      'label' => 'Torrent File',
      'required' => 1,
      'delta' => '-1',
      'id' => 'field_torrent_file_fid',
      'table' => 'node_data_field_torrent_file',
      'field' => 'field_torrent_file_fid',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('fields', array(
    'type' => array(
      'label' => 'Type',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'exclude' => 0,
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
    'tid' => array(
      'label' => 'All terms',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'type' => 'separator',
      'separator' => ', ',
      'empty' => '',
      'link_to_taxonomy' => 1,
      'limit' => 0,
      'vids' => array(
        '1' => 0,
      ),
      'exclude' => 1,
      'id' => 'tid',
      'table' => 'term_node',
      'field' => 'tid',
      'relationship' => 'none',
    ),
    'title' => array(
      'label' => 'Title',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 1,
      'exclude' => 1,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'none',
    ),
    'php' => array(
      'label' => 'Title',
      'code_style' => 'code_block',
      'output_source' => 'result',
      'php_code' => 'return $output->title.\'<div style="font-size: 4pt;">\'.$output->tid.\'</div>\';',
      'exclude' => '',
      'id' => 'php',
      'table' => 'views_php_extension',
      'field' => 'php',
      'relationship' => 'none',
    ),
    'total_size' => array(
      'label' => 'Size',
      'alter' => array(
        'alter_text' => 1,
        'text' => '<span class="bf_profile_size">[total_size]</span>',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'file_size_display' => 'formatted',
      'exclude' => 0,
      'id' => 'total_size',
      'table' => 'bt_torrents',
      'field' => 'total_size',
      'relationship' => 'field_torrent_file_fid',
    ),
    'seeders' => array(
      'label' => '<img src="'.$base_path.'/seeder.png" title="Seeders"/>',
      'alter' => array(
        'alter_text' => 1,
        'text' => '<span class="bf_profile_count">[seeders]</span>',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'exclude' => 0,
      'id' => 'seeders',
      'table' => 'bt_torrents',
      'field' => 'seeders',
      'relationship' => 'field_torrent_file_fid',
    ),
    'leechers' => array(
      'label' => '<img src="'.$base_path.'/leecher.png" title="Leechers"/>',
      'alter' => array(
        'alter_text' => 1,
        'text' => '<span class="bf_profile_count">[leechers]</span>',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'exclude' => 0,
      'id' => 'leechers',
      'table' => 'bt_torrents',
      'field' => 'leechers',
      'relationship' => 'field_torrent_file_fid',
    ),
    'snatches' => array(
      'label' => '<img src="'.$base_path.'/snatches.png" title="Snatches"/>',
      'alter' => array(
        'alter_text' => 1,
        'text' => '<span class="bf_profile_count">[snatches]</span>',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'exclude' => 0,
      'id' => 'snatches',
      'table' => 'bt_torrents',
      'field' => 'snatches',
      'relationship' => 'field_torrent_file_fid',
    ),
    'downloaded' => array(
      'label' => '<img src="'.$base_path.'/downloaded_count.png" title="Downloaded"/>',
      'alter' => array(
        'alter_text' => 1,
        'text' => '<span class="bf_profile_count">[downloaded]</span>',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'exclude' => 0,
      'id' => 'downloaded',
      'table' => 'bt_torrents',
      'field' => 'downloaded',
      'relationship' => 'field_torrent_file_fid',
    ),
    'last_seed' => array(
      'label' => 'Last Seed',
      'alter' => array(
        'alter_text' => 1,
        'text' => '<span class="bf_profile_datetime">[last_seed]</span>',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'date_format' => 'custom',
      'custom_date_format' => 'Y.m.d H:i',
      'exclude' => 0,
      'id' => 'last_seed',
      'table' => 'bt_torrents',
      'field' => 'last_seed',
      'relationship' => 'field_torrent_file_fid',
    ),
    'bytes_uploaded' => array(
      'label' => '<img src="'.$base_path.'/uploaded.png" title="Bytes Uploaded"/>',
      'alter' => array(
        'alter_text' => 1,
        'text' => '<span class="bf_profile_size">[bytes_uploaded]</span>',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'file_size_display' => 'formatted',
      'exclude' => 0,
      'id' => 'bytes_uploaded',
      'table' => 'bt_tracker_connections',
      'field' => 'bytes_uploaded',
      'relationship' => 'field_torrent_file_fid',
    ),
    'bytes_downloaded' => array(
      'label' => '<img src="'.$base_path.'/downloaded.png" title="Bytes Downloaded"/>',
      'alter' => array(
        'alter_text' => 1,
        'text' => '<span class="bf_profile_size">[bytes_downloaded]</span>',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'file_size_display' => 'formatted',
      'exclude' => 0,
      'id' => 'bytes_downloaded',
      'table' => 'bt_tracker_connections',
      'field' => 'bytes_downloaded',
      'relationship' => 'field_torrent_file_fid',
    ),
    'php_1' => array(
      'label' => '<img src="'.$base_path.'/ratio.png" title="Ratio"/>',
      'code_style' => 'code_block',
      'output_source' => 'output',
      'php_code' => '?><span class="bf_profile_ratio"><?php
  if ($row->bytes_downloaded == 0) {
      if ($row->bytes_uploaded != 0) {
        print(\'∞\');
      } else {
        print(\'\');
      }
    } else {
      print(round($row->bytes_uploaded/$row->bytes_downloaded, 2));
    }
  ?></span><?php
  ',
      'exclude' => '',
      'id' => 'php_1',
      'table' => 'views_php_extension',
      'field' => 'php',
      'relationship' => 'none',
    ),
    'bytes_left' => array(
      'label' => '<img src="'.$base_path.'/left.png" title="Bytes Left"/>',
      'alter' => array(
        'alter_text' => 1,
        'text' => '<span class="bf_profile_size">[bytes_left]</span>',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'file_size_display' => 'formatted',
      'exclude' => 0,
      'id' => 'bytes_left',
      'table' => 'bt_tracker_connections',
      'field' => 'bytes_left',
      'relationship' => 'field_torrent_file_fid',
    ),
    'upload_speed' => array(
      'label' => '<img src="'.$base_path.'/upload_speed.png" title="Upload Speed"/>',
      'code_style' => 'expression',
      'output_source' => 'result',
      'php_code' => '\'<span class="bf_profile_speed">\'.bt_format_speed($field).\'</span>\'',
      'exclude' => 0,
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
      ),
      'id' => 'upload_speed',
      'table' => 'bt_tracker_connections',
      'field' => 'upload_speed',
      'relationship' => 'field_torrent_file_fid',
    ),
    'download_speed' => array(
      'label' => '<img src="'.$base_path.'/download_speed.png" title="Download Speed"/>',
      'code_style' => 'expression',
      'output_source' => 'result',
      'php_code' => '\'<span class="bf_profile_speed">\'.bt_format_speed($field).\'</span>\'',
      'exclude' => 0,
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
      ),
      'id' => 'download_speed',
      'table' => 'bt_tracker_connections',
      'field' => 'download_speed',
      'relationship' => 'field_torrent_file_fid',
    ),
    'php_2' => array(
      'label' => '<img src="'.$base_path.'/speed_ratio.png" title="Speed Ratio"/>',
      'code_style' => 'code_block',
      'output_source' => 'output',
      'php_code' => '?><span class="bf_profile_ratio"><?php
  if ($row->download_speed == 0) {
      if ($row->upload_speed != 0) {
        print(\'∞\');
      } else {
        print(\'\');
      }
    } else {
      print(round($row->upload_speed/$row->download_speed, 2));
    }
  ?></span><?php
  ',
      'exclude' => '',
      'id' => 'php_2',
      'table' => 'views_php_extension',
      'field' => 'php',
      'relationship' => 'none',
    ),
    'last_announce' => array(
      'label' => 'Last Announce',
      'alter' => array(
        'alter_text' => 1,
        'text' => '<span class="bf_profile_datetime">[last_announce]</span>',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'date_format' => 'custom',
      'custom_date_format' => 'Y.m.d H:i',
      'exclude' => 0,
      'id' => 'last_announce',
      'table' => 'bt_tracker_connections',
      'field' => 'last_announce',
      'relationship' => 'field_torrent_file_fid',
    ),
  ));
  $handler->override_option('arguments', array(
    'uid' => array(
      'default_action' => 'empty',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => '',
      'breadcrumb' => '',
      'default_argument_type' => 'fixed',
      'default_argument' => '',
      'validate_type' => 'user',
      'validate_fail' => 'empty',
      'break_phrase' => 0,
      'not' => 0,
      'id' => 'uid',
      'table' => 'bt_tracker_connections',
      'field' => 'uid',
      'relationship' => 'field_torrent_file_fid',
      'default_options_div_prefix' => '',
      'default_argument_user' => 0,
      'default_argument_fixed' => '',
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'event' => 0,
        'page' => 0,
        'profile' => 0,
        'pseudo_torrent' => 0,
        'story' => 0,
        'torrent' => 0,
        'torrent_2' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(),
      'validate_argument_type' => 'tid',
      'user_argument_type' => 'uid',
      'restrict_user_roles' => 0,
      'user_roles' => array(),
      'validate_argument_php' => '',
      'validate_user_argument_type' => 'uid',
      'validate_user_roles' => array(
        '2' => 0,
      ),
      'validate_argument_transform' => 0,
      'validate_user_restrict_roles' => 0,
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 0,
    'order' => 'asc',
    'columns' => array(
      'type' => 'type',
      'title' => 'title',
      'downloaded' => 'downloaded',
      'last_seed' => 'last_seed',
      'leechers' => 'leechers',
      'seeders' => 'seeders',
      'snatches' => 'snatches',
      'total_size' => 'total_size',
    ),
    'info' => array(
      'type' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'title' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'downloaded' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'last_seed' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'leechers' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'seeders' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'snatches' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'total_size' => array(
        'sortable' => 1,
        'separator' => '',
      ),
    ),
    'default' => '-1',
  ));
  $views[$view->name] = $view;

  $base_view = $view;

  $view = unserialize(serialize($base_view));
  $view->name = 'bt_user_seeding_torrents';
  $view->description = 'View to set as "Seeding torrents view" at admin page';
  $handler = $view->display['default']->handler; 
  $handler->override_option('filters', array(
    'bytes_left' => array(
      'operator' => '=',
      'value' => array(
        'value' => '0',
        'min' => '',
        'max' => '',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'bytes_left',
      'table' => 'bt_tracker_connections',
      'field' => 'bytes_left',
      'relationship' => 'field_torrent_file_fid',
    ),
  ));
  $views[$view->name] = $view;
  
  $view = unserialize(serialize($base_view));
  $view->name = 'bt_user_leeching_torrents';
  $view->description = 'View to set as "Leeching torrents view" at admin page';
  $handler = $view->display['default']->handler; 
  $handler->override_option('filters', array(
    'bytes_left' => array(
      'operator' => '>',
      'value' => array(
        'value' => '0',
        'min' => '',
        'max' => '',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'bytes_left',
      'table' => 'bt_tracker_connections',
      'field' => 'bytes_left',
      'relationship' => 'field_torrent_file_fid',
    ),
  ));
  $views[$view->name] = $view;
  
  return $views;
}
