<?php

/**
 * @file
 * The wrapper for scrape portion of the tracker module.
 */

require_once('includes/common.inc');
require_once('includes/bootstrap.inc');
drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);

require_once(drupal_get_path('module', 'bt_tracker').'/../includes/scrape_core.php');
