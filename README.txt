BitTorrent module pack provides two items:
- A BitTorrent widget for file field type (bt_torrent)
- A BitTorrent tracker (bt_tracker)

[Features]

Torrent widget:
 - Assign Torrent widget to any file field of any content type
 - Upload only one or unlimited amount of torrent-files per one file field with torrent widget
   (in case for an example of adding subtitles to release or adding new episodes to TV series release,
   instead of reupload the torrent-file (and force other users to redownload new torrent-file) you can just append a new torrent-file to a release)
 - Specify a description for each uploaded torrent-file
 - Automatically append or override announce-URL list from torrent-file with own announce-URL or leave it intact (support "announce-list" extension).
   (Appending and overriding are make at moment of torrent-file download, so there is no need to reupload all torrent-files in case of own announce-URL or site domen name change).

Other:
 - Scrape other trackers for statistics
 - Support torrent-files without any announce URL

Security:
 - Specify which roles are allowed to download torrent-files and use the tracker ("download torrent" permission).

Tracker:
 - Track any torrent that has been uploaded to the site.
 - The administrator can enforce share ratios and maximum number of simultaneous downloads.

Accessibility & Statistics:
Tracker operates within one of three "scopes". The scopes determine the amount of statistics collected as well as the level of security for the tracker.
 - "Public" scope allows anyone to use the tracker and minimal statistics are collected.
 - "Mixed" scope still allows anyone to connect to the tracker, but the users who connect with a passkey have specifics collected about their tracker usage (upload and download amounts).
 - "Private" scope requires a passkey and collects the same statistics as the mixed scope.

View integration:
 - minimal support for views integration

The web-seeding extension to the BitTorrent protocol has also been implemented allowing the server to act as a seed when no other seeds are available.
The module implments both the GetRight and BitTornado specifications to support a larger audience of BitTorrent clients.
(D6 port currently does not support web-seeding).

[Require]
 - CCK ( http://drupal.org/project/cck )
 - FileField ( http://drupal.org/project/filefield )

[Support]
 - Views ( http://drupal.org/project/views )
