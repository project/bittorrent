[BitTorrent (all versions)]
 - be sure that no one access site during update: put site into maintenence mode, temporarily remove "announce.php" and "scrape.php" files from Drupal root dir   

[BitTorrent 6.x-9.2-alpha6]
Before updating please:
 - enable "token" module ( http://drupal.org/project/token )

[BitTorrent 6.x-9.2-alpha5]
Before updating please:
 - enable "settings" module ( http://drupal.org/project/settings )

[BitTorrent 6.x-9.2-alpha4]
Don't use this one. 
 
[BitTorrent 6.x-9.2-alpha3] 
You can update from one of these versions:
6.x-9.0-beta1	2009-Feb-26
6.x-9.x-dev	2009-Mar-27
and later...

To update from "6.x-9.0-beta1" or "6.x-9.x-dev" follow these steps:
0) Goto "admin/build/modules" and enable "FileField" module.
1) Delete whole existent "bittorrent" folder (from "modules" folder) and delete "announce.php", "scrape.php" and "bt_common.inc" from drupal site root folder.
2) Copy new "bittorrent" folder to the same place (into "modules" folder) and copy new "announce.php" and "scrape.php" to drupal site root folder.
3) Open "update.php" ( http://example.com/update.php ) in your browser.
4) Follow to instructions. Then press "Continue" button.
5) Expand "Select versions" group and check that "bt_torrent module" set to "6900" and "bt_tracker module" set to "6900".
6) Press "Update" button.

[BitTorrent 6.x-9.2-alpha2]
Don't use this one. 
