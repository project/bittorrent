<?php

/**
 * @file
 * Wrapper for scrape functionality.
 */

// Require the bt functions
require_once('bt_common.inc');

// Uncomment for debug
//set_error_handler('announce_error_handler');

// Include bootstrap.inc and run the bootstrap
require_once('includes/bootstrap.inc');
drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);

// Include "module.inc" because it's used by "user" module. We use it also.
require_once('includes/module.inc');

// Refresh module list (because "user" module use "module_list")
module_list(TRUE);

// for "user_load" function
drupal_load('module', 'user');

// Include settings functions
module_load_include('inc', 'settings', 'settings.api');

// Include "bt_tracker" module settings
module_load_include('inc', 'bt_tracker', 'bt_tracker.settings');

// Initializes $conf so we can use variable_get & settings_get
$conf = variable_init(isset($conf) ? $conf : array());

// Include "bt_tracker" "bt_tracker_announce"
module_load_include('inc', 'bt_tracker', 'bt_tracker_announce');

bt_tracker_announce();
