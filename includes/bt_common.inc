<?php

/**
 * @file
 * The bencoding and bencdecoding functions for the bittorrent module.
 */

//666
// For debug purpose only... //TODO: remove
function tools_log($value, $log_file_name = 'common.log') {
  $file = fopen($log_file_name, 'ab');
  fwrite($file, print_r($value, TRUE)."\n");
  fclose($file);
}
//666

function bittorrent_log(/* Unlimited amount of arguments */) {
  $values = func_get_args();
  
  $file = fopen(drupal_get_path('module', 'bt_torrent').'/../BitTorrent.log', 'ab');
  fwrite($file, '['.format_date(time(), 'custom', 'Y-m-d H:i:s', 0, 'en').']'."\n");
  foreach ($values as $value) {
    fwrite($file, print_r($value, TRUE)."\n");
  }
  fclose($file);
}

function announce_error_handler($errno, $errstr, $errfile = NULL, $errline = NULL, $errcontext = NULL) {
  if ($errno == E_NOTICE) {
    return;
  }
  
  $error_message = $errstr .'. '. $errfile .' : '. $errline;
  
  bittorrent_log($error_message);
  //bt_error($error_message);
}

function bittorrent_images($name = NULL) {
  static $images = NULL;
  
  $base_images_path = '/'.dirname(drupal_get_path('module', 'bt_torrent')).'/images/';
  
  if (!isset($images)) {
    $images = array(
      'seeders'          => 'seeder.png',
      'leechers'         => 'leecher.png',
      'snatches'         => 'snatches.png',
      'downloaded_count' => 'downloaded_count.png',
      'uploaded'         => 'uploaded.png',
      'downloaded'       => 'downloaded.png',
      'ratio'            => 'ratio.png',
      'left'             => 'left.png',
      'upload_speed'     => 'upload_speed.png',
      'download_speed'   => 'download_speed.png',
      'speed_ratio'      => 'speed_ratio.png',
    
      'download'         => 'download.png',
    );
    
    foreach ($images as &$image) {
      $image = $base_images_path.$image; 
    }
  }

  if (isset($name)) {
    return $images[$name];
  }
  else {
    return $images;
  }  
}

function bittorrent_images_titles($name = NULL) {
  static $titles = NULL;
  
  if (!isset($titles)) {
    $titles = array(
      'seeders'          => t('Seeders'),
      'leechers'         => t('Leechers'),
      'snatches'         => t('Snatches'),
      'downloaded_count' => t('Downloaded Count'),
      'uploaded'         => t('Uploaded'),
      'downloaded'       => t('Downloaded'),
      'ratio'            => t('Ratio'),
      'left'             => t('Left'),
      'upload_speed'     => t('Upload Speed'),
      'download_speed'   => t('Download Speed'),
      'speed_ratio'      => t('Speed Ratio'),
    
      'download'         => t('Download'),
    );
  }

  if (isset($name)) {
    return $titles[$name];
  }
  else {
    return $titles;
  }  
}

function bittorrent_image_html($name) {
  $title = bittorrent_images_titles($name);
  return '<img src="'.bittorrent_images($name).'" alt="'.$title.'" title="'.$title.'"/>';
}

/**
 * Convert textual representation of IP address (IPv4 or IPv6) to its binary representation.
 * IPv6 is fully (all its three forms) supported (RFC 3513).
 *
 * @param $ip
 *   Textual representation of IP address (IPv4 or IPv6).
 * @return
 *   Binary representation of given IP.
 */
function bt_ip_text2bin($ip) {
  if (strpos($ip, ':') === FALSE) {
    // IPv4
    $ip_long = ip2long($ip);
   
    if ($ip_long === FALSE) {
      return FALSE;
    }

    return pack('N', $ip_long);
  } else {
    // IPv6
    $sides = explode('::', $ip);
    
    if (count($sides) > 2) {
      return FALSE;
    }
    
    $parts = explode(':', $sides[0]);
     
    if (count($sides) == 2) {
      $fill_index = count($parts); 
      $parts = array_merge($parts, explode(':', $sides[1]));
    }
    
    $last = end($parts); 
    if (strpos($last, '.') !== FALSE) {
      $last = array_pop($parts); 
      $last_long = ip2long($last);
      
      if ($last_long === FALSE) {
        return FALSE;
      }

      array_push($parts, bin2hex(pack('n', $last_long >> 16)));
      array_push($parts, bin2hex(pack('n', $last_long & 0xFFFF)));
    }
    
    if (isset($fill_index)) {
      $fill_count = 8 - count($parts);
    
      if ($fill_count <= 0) {
        return FALSE;
      }

      array_splice($parts, $fill_index, 0, array_fill(0, $fill_count, '0'));
    }
    
    if (count($parts) != 8) {
      return FALSE; 
    }
    
    foreach ($parts as &$part) {
      if (strlen($part) > 4) {
        return FALSE;
      }
      
      $part = str_pad($part, 4, '0', STR_PAD_LEFT);
    }
    
    return pack('H*', join('', $parts));
  }
}

/**
 * Convert binary representation of IP address (IPv4 or IPv6) to its textual representation.
 * IPv6 is fully (all its three forms) supported (RFC 3513).
 *
 * @param $ip
 *   Binary representation of IP address.
 * @return
 *   Textual representation of given IP.
 */
function bt_ip_bin2text($ip, $no_leading_zeros = FALSE, $ipv6_compress = FALSE, $ipv6_mixed = FALSE) {
  $ip_len = strlen($ip);
  if ($ip_len == 4) {
    // IPv4
    list(, $ip) = unpack('N', $ip);
    return long2ip($ip);
  } else if ($ip_len == 16) {
    // IPv6
    if ($ipv6_mixed) {
      list(, $ip4_part) = unpack('N', substr($ip, strlen($ip) - 4, 4));
      $ip4_part = long2ip($ip4_part);
      $ip = substr($ip, 0, 16 - 4);
    }
    
    if ($ipv6_compress) {
      for( $i = 0; $i <= strlen($ip)/2 - 1 + 1; $i++ ) {
        $part = substr($ip, $i*2, 2);
        if ($part == "\0\0") {
          if (!isset($compress_index)) {
            $compress_index = $i;
            $compress_count = 1;
          } else {
            $compress_count++;
          }
        } else {
          if (isset($compress_index)) {
            if (!isset($saved_compress_count) || ($compress_count > $saved_compress_count)) {            
              $saved_compress_index = $compress_index;
              $saved_compress_count = $compress_count;
            }
            unset($compress_index);
          }
        }
      }
    }

    if (isset($saved_compress_index)) {
      $sides = array(
        substr($ip, 0, $saved_compress_index*2), 
        substr($ip, ($saved_compress_index + $saved_compress_count)*2, strlen($ip))
      );
    } else {
      $sides = array($ip);
    }

    $side_text_list = array();
    foreach ($sides as $side) {
      $part_text_list = array();
      for( $i = 0; $i <= strlen($side)/2 - 1; $i++ ) {
        $part = substr($side, $i*2, 2);
        list(, $part_text) = unpack('H*', $part);
        if ($no_leading_zeros) {
          $part_text = ltrim($part_text, '0');
          if (strlen($part_text) == 0) {
            $part_text = '0';
          }
        }
        $part_text_list[] = $part_text; 
      }
      $side_text_list[] = join(':', $part_text_list); 
    }
    $ip_text = join('::', $side_text_list);
    
    if ($ipv6_mixed) {
      if ($ip_text{strlen($ip_text) - 1} != ':') {
        $ip_text .= ':';
      }
      
      $ip_text .= $ip4_part;
    }
    
    return $ip_text;
  } else {
    return FALSE;
  }
}

/**
 * Generate a string representation for the given byte count.
 *
 * @param $size
 *   A size in bytes.
 * @param $langcode
 *   Optional language code to translate to a language other than what is used
 *   to display the page.
 * @return
 *   A translated string representation of the size.
 */
// Taken from "includes/common.inc" of Drupal 7 because "format_size" of Drupal 6 does not support measures larger than MB (GB, TB, ...)
// In D7 port this function must be deleted and all it's calls replaced with standard "format_size"
define('DRUPAL_KILOBYTE', 1024);
function bt_format_size($size, $langcode = NULL) {
  if ($size < DRUPAL_KILOBYTE) {
    return format_plural($size, '1 byte', '@count bytes', array(), $langcode);
  }
  else {
    $size = $size / DRUPAL_KILOBYTE; // Convert bytes to kilobytes.
    $units = array(
      t('@size KB', array(), $langcode),
      t('@size MB', array(), $langcode),
      t('@size GB', array(), $langcode),
      t('@size TB', array(), $langcode),
      t('@size PB', array(), $langcode),
      t('@size EB', array(), $langcode),
      t('@size ZB', array(), $langcode),
      t('@size YB', array(), $langcode),
    );
    foreach ($units as $unit) {
      if (round($size, 2) >= DRUPAL_KILOBYTE) {
        $size = $size / DRUPAL_KILOBYTE;
      }
      else {
        break;
      }
    }
    return str_replace('@size', round($size, 2), $unit);
  }
}

/**
 * Generate a string representation for the given speed.
 *
 * @param $speed
 *   A speed in bytes/s.
 * @param $langcode
 *   Optional language code to translate to a language other than what is used to display the page.
 * @return
 *   A translated string representation of the speed.
 */
// Taken from D7 "format_size()" with some changes. Allow to show speed.
function bt_format_speed($speed, $langcode = NULL) {
  $units = array(
    t('@speed B/s', array(), $langcode),
    t('@speed KB/s', array(), $langcode),
    t('@speed MB/s', array(), $langcode),
    t('@speed GB/s', array(), $langcode),
    t('@speed TB/s', array(), $langcode),
    t('@speed PB/s', array(), $langcode),
    t('@speed EB/s', array(), $langcode),
    t('@speed ZB/s', array(), $langcode),
    t('@speed YB/s', array(), $langcode),
  );
  foreach ($units as $unit) {
    if (round($speed, 2) >= DRUPAL_KILOBYTE) {
      $speed = $speed / DRUPAL_KILOBYTE;
    }
    else {
      break;
    }
  }
  return str_replace('@speed', round($speed, 2), $unit);
}

function bt_format_ratio($uploaded, $downloaded) {
  if ($downloaded == 0) {
    if ($uploaded != 0) {
      return '∞';
    } else {
      return '';
    }
  } else {
    return round($uploaded/$downloaded, 2);
  }
}

function strip_magic($x)
{
  return (get_magic_quotes_gpc() ? stripslashes($x) : $x);
}

/**
 * Convert IP (v4 or v6 formatted) (if it is valid) to a 32 byte hex string representing that address.
 *
 * Requires php >= 5.2 as it uses the "filter_var" function.
 */
// Taken from http://ru2.php.net/manual/ru/function.ip2long.php#82013 with modifications
/*
function ip_to_hex($ip_address) {
  //if( !filter_var($ip_address, FILTER_VALIDATE_IP) )
  //  return false;

  $is_IPv6 = false;
  $is_IPv4 = false;
  if( filter_var($ip_address, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) !== false )
    $is_IPv6 = true;
  else if(filter_var($ip_address, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) !== false)
    $is_IPv4 = true;

  if( !$is_IPv4 && !$is_IPv6 )
    return false;

  $hex = '';

  // IPv4 format
  if( $is_IPv4 )
    $hex = str_pad(dechex(ip2long($ip_address)), 8, '0', STR_PAD_LEFT);
  // IPv6 format
  else
  {
    $parts = explode(':', $ip_address);

    // If this is mixed IPv6/IPv4, convert end to IPv6 value
    if( filter_var(end($parts), FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) !== false )
    {
      $partsV4 = explode('.', end($parts));
      for($i = 0; $i < 4; $i++) {
          $partsV4[$i] = str_pad(dechex($partsV4[$i]), 2, '0', STR_PAD_LEFT);
      }

      $parts[count($parts) - 1] = $partsV4[0].$partsV4[1];
      $parts[] = $partsV4[2].$partsV4[3];
    }

    $numMissing = 8 - count($parts);
    $expandedParts = array();
    $expansionDone = false;
    foreach( $parts as $part )
      if(!$expansionDone && $part == '')
      {
          for($i = 0; $i <= $numMissing; $i++)
              $expandedParts[] = '0000';

          $expansionDone = true;
      }
      else
        $expandedParts[] = $part;

    foreach($expandedParts as &$part) {
        $part = str_pad($part, 4, '0', STR_PAD_LEFT);
    }
    $hex = join('', $expandedParts);
  }

  return strtolower(str_pad($hex, 32, '0', STR_PAD_LEFT));
}
*/

/**
 * Sends a message to the bittorrent client and break script execution.
 *
 * @param $msg
 *   The error message to be displayed to the client.
 */
function bt_error($msg) {
  $respone = array('failure reason' => $msg);
  bencode_response_raw(bencode($respone));
  exit;
}

/**
 * Converts a specified structure into its bencoded form. Acceptable value include: integers, strings, arrays, and assoc. arrays.
 * Note that associative arrays are encoded into dictionaries while non-associative arrays are encoded into lists
 *
 * @param $struct
 *    This is the structure to bencode example passing the value 'hi' would return '2:hi'
 * @return The bencoded form of $struct
 */
function bencode($struct) {
  if (is_array($struct)) {
    $numeric = true;
    foreach(array_keys($struct) as $key) {
      if (!is_numeric($key)) {
        $numeric = false;
        break;
      }
    }

    if (!$numeric) {
      $out = 'd';
      ksort($struct);
      foreach($struct as $key => $value) {
        $out .= bencode($key);
        $out .= bencode($value);
      }
      $out .= 'e';
    }
    else {
      $out = 'l';
      ksort($struct);
      foreach($struct as $key => $value) {
        $out .= bencode($value);
      }
      $out .= 'e';
    }

    return $out;
  }
  elseif (preg_match('/^(\+|\-)?\d+$/', $struct)) {
    return 'i'.$struct.'e';
  }
  else {
    return strlen($struct).':'.$struct;
  }
  return;
}

/**
 * Decodes bencoded data. It automatically determines type and create the structure based off of the type.
 *
 * @param $metadata
 *    The data to be decoded
 * @return The decoded data (or NULL if error):
 */
function bdecode(&$metadata, &$position = 0) {
  switch ($metadata{$position}) {
    case 'i':
      $position++;
      $end_pos = strpos($metadata, 'e', $position);
      if ($end_pos !== FALSE) {
        $value = substr($metadata, $position, $end_pos - $position);
        /*if (!ctype_digit($value)) {
          trigger_error('int: not a ctype_digit', E_USER_ERROR); 
          return;
        }*/
        if (($value === '-0') || (($value{0} == '0') && (strlen($value) != 1))) {
          //trigger_error('int: wrong 0 format', E_USER_ERROR); 
          return;
        }
        $position = $end_pos + 1;
        return (int)$value; 
      }
      else {
        //trigger_error('int: corrupted value', E_USER_ERROR); 
        return;
      }
    case 'l':
      $length = strlen($metadata);
      $position++;
      $list = array();
      for (;;) {
        if ($position >= $length) {
          //trigger_error('list: corrupted value', E_USER_ERROR); 
          return;
        }
        if ($metadata{$position} == 'e') {
          break;
        }
        
        $item_value = bdecode($metadata, $position);
        if (!isset($item_value)) {
          return;
        }//*/
        $list[] = $item_value;
      }
      $position++;
      return $list;
    case 'd':
      $length = strlen($metadata);
      $position++;
      $dictionary = array();
      for (;;) {
        if ($position >= $length) {
          //trigger_error('dictionary: corrupted value', E_USER_ERROR); 
          return;
        }
        if ($metadata{$position} == 'e') {
          break;
        }
        
        $item_key = bdecode($metadata, $position);
        if (!isset($item_key) || !is_string($item_key)) {
          //trigger_error('dictionary: key is not a string value', E_USER_ERROR); 
          return;
        }
        $item_value = bdecode($metadata, $position);
        if (!isset($item_value)) {
          return;
        }//*/
        $dictionary[$item_key] = $item_value;
      }
      $position++;
      return $dictionary;
    default:
      $colon_pos = strpos($metadata, ':', $position);
      if ($colon_pos !== FALSE) {
        $length = substr($metadata, $position, $colon_pos - $position);
        if (!ctype_digit($length)) {
          return;
          //trigger_error('string: length is not a ctype_digit', E_USER_ERROR); 
        }
        $position = $colon_pos + 1;
        $value = substr($metadata, $position, $length);
        if (strlen($value) != $length) {
          //trigger_error('string: wrong length value', E_USER_ERROR); 
          return;
        }
        $position += $length;
        return (string)$value;
      }
      else {      
        //trigger_error('unknown value format', E_USER_ERROR); 
        return;
      }
  }
}

/**
 * This funtion decodes bencoded data. It automatically determines type and create the structure based off of the type.
 *
 * @param $metadata
 *    The data to be decoded
 * @return The decoded data in the following structure (or NULL if error):
 *    array(
 *      ['type'] => type,              // This can be integer, string, list, dictionary
 *      ['value'] => value,            // This is the decoded value
 *      ['strlen'] => strlen,          // This is the length of the bencoded string
 *      ['string'] => bstring,         // This is the bencoded string
 *    )
 */
function bdecode_ex($metadata) {
  if (preg_match('/^(\d+):/', $metadata, $matches)) {
    $length = $matches[1];
    $string_start = strlen($length) + 1;
    $value = substr($metadata, $string_start, $length);
    $bencoded = substr($metadata, 0, $string_start + $length);
    if (strlen($value) != $length) {
      return;
    }
    return array('type' => 'string', 'value' => $value, 'strlen' => strlen($bencoded), 'string' => $bencoded);
  }

  if (preg_match('/^i(\d+)e/', $metadata, $matches)) {
    $value = $matches[1];
    $bencoded = 'i' . $value . 'e';
    if ($value === '-0') {
      return;
    }
    if ($value[0] == '0' && strlen($value) != 1) {
      return;
    }
    return array('type' => 'integer', 'value' => $value, 'strlen' => strlen($bencoded), 'string' => $bencoded);
  }

  switch ($metadata[0]) {
    case 'l':
      return bdecode_list_ex($metadata);

    case 'd':
      return bdecode_dictionary_ex($metadata);

    default:
      return;
  }
}

/**
 * Used to decode bencoded lists
 *
 * @param $metadata
 *    The bencoded list
 * @return An array containing the contents of the list or NULL if error.
 */
function bdecode_list_ex($metadata) {
  if ($metadata[0] != 'l') {
    return;
  }

  $length = strlen($metadata);
  $position = 1;
  $value = array();
  $bencoded = 'l';

  for (;;) {
    if ($position >= $length) {
      return;
    }
    if ($metadata[$position] == 'e') {
      break;
    }
    $return = bdecode_ex(substr($metadata, $position));
    if (!isset($return) || !is_array($return)) {
      return;
    }
    $value[] = $return;
    $position += $return['strlen'];
    $bencoded .= $return['string'];
  }

  $bencoded .= 'e';
  return array('type' => 'list', 'value' => $value, 'strlen' => strlen($bencoded), 'string' => $bencoded);
}

/**
 * Used to decode bencoded dictionaries
 *
 * @param $metadata
 *    The bencoded dctionary
 * @return An associative array containing the keys and values of the dictionary or NULL if error.
 */
function bdecode_dictionary_ex($metadata) {
  if ($metadata[0] != 'd') {
    return;
  }
  $length = strlen($metadata);
  $position1 = 1;
  $value = array();
  $bencoded = 'd';
  for (;;) {
    if ($position1 >= $length) {
      return;
    }
    if ($metadata[$position1] == 'e') {
      break;
    }
    $return = bdecode_ex(substr($metadata, $position1));
    if (!isset($return) || !is_array($return) || $return['type'] != 'string') {
      return;
    }
    $position2 = $return['value'];
    $position1 += $return['strlen'];
    $bencoded .= $return['string'];
    if ($position1 >= $length) {
      return;
    }
    $return = bdecode_ex(substr($metadata, $position1));
    if (!isset($return) || !is_array($return)) {
      return;
    }
    $value[$position2] = $return;
    $position1 += $return['strlen'];
    $bencoded .= $return['string'];
  }
  $bencoded .= 'e';
  return array('type' => 'dictionary', 'value' => $value, 'strlen' => strlen($bencoded), 'string' => $bencoded);
}

/**
 * Strips the unnescessary values from the bdecode_ex() result.
 *
 * @param $bparsed
 *   The array of data returned by bdecode_ex()
 * @return The modified structure without the 'value' field (or NULL if error). Example:
 *    array(
 *      'announce' => 'announce url',
 *      'creation date' => 'creation date',
 *      'info' => array(
 *        'files' => 'files list',
 *      'name' => 'torrent name',
 *        'piece length' => 'length of each piece',
 *        'pieces' => 'The piece hashes',
 *      ),
 *    )
 */
function strip_excess($bparsed) {
  if (is_array($bparsed)) {
    foreach($bparsed as $key => $value) {
      if (is_array($value)) {
        if (array_key_exists('value', $value)) {
          $bparsed[$key] = strip_excess($value['value']);
        }
        else {
          $bparsed[$key] = strip_excess($value);
        }
      }
    }
  }

  if (is_array($bparsed) && array_key_exists('value', $bparsed)) {
    return $bparsed['value'];
  }
  else {
    return $bparsed;
  }
}

function torrent_compose_client_name_by_str($peer_id_str) {
  return torrent_compose_client_name(pack('H*', $peer_id_str));
}

function torrent_compose_client_name($peer_id) {
  // Azures client naming format
  if( substr($peer_id, 0, 1) == '-' )
  {
    $azureus_client_code_list = array('AG' => 'Ares', 'A~' => 'Ares', 'AR' => 'Arctic', 'AT' => 'Artemis', 'AX' => 'BitPump', 'AZ' => 'Azureus', 'BB' => 'BitBuddy', 'BC' => 'BitComet', 'BF' => 'Bitflu', 'BG' => 'BTG (uses Rasterbar libtorrent)', 'BP' => 'BitTorrent Pro (Azureus + spyware)', 'BR' => 'BitRocket', 'BS' => 'BTSlave', 'BW' => 'BitWombat', 'BX' => '~Bittorrent X', 'CD' => 'Enhanced CTorrent', 'CT' => 'CTorrent', 'DE' => 'DelugeTorrent', 'DP' => 'Propagate Data Client', 'EB' => 'EBit', 'ES' => 'electric sheep', 'FC' => 'FileCroc', 'FT' => 'FoxTorrent', 'GS' => 'GSTorrent', 'HL' => 'Halite', 'HN' => 'Hydranode', 'KG' => 'KGet', 'KT' => 'KTorrent', 'LC' => 'LeechCraft', 'LH' => 'LH-ABC', 'LP' => 'Lphant', 'LT' => 'libtorrent', 'lt' => 'libTorrent', 'LW' => 'LimeWire', 'MO' => 'MonoTorrent', 'MP' => 'MooPolice', 'MR' => 'Miro', 'MT' => 'MoonlightTorrent', 'NX' => 'Net Transport', 'OT' => 'OmegaTorrent', 'PD' => 'Pando', 'qB' => 'qBittorrent', 'QD' => 'QQDownload', 'QT' => 'Qt 4 Torrent example', 'RT' => 'Retriever', 'RZ' => 'RezTorrent', 'S~' => 'Shareaza alpha/beta', 'SB' => '~Swiftbit', 'SS' => 'SwarmScope', 'ST' => 'SymTorrent', 'st' => 'sharktorrent', 'SZ' => 'Shareaza', 'TN' => 'TorrentDotNET', 'TR' => 'Transmission', 'TS' => 'Torrentstorm', 'TT' => 'TuoTu', 'UL' => 'uLeecher!', 'UM' => '�Torrent for Mac', 'UT' => 'uTorrent', 'VG' => 'Vagaa', 'WT' => 'BitLet', 'WY' => 'FireTorrent', 'XL' => 'Xunlei', 'XT' => 'XanTorrent', 'XX' => 'Xtorrent', 'ZT' => 'ZipTorrent');

    $client_code = substr($peer_id, 1, 2);
    if( isset($azureus_client_code_list[$client_code]) )
    {
      $client_name = $azureus_client_code_list[$client_code];
      $version = substr($peer_id, 3, 4);
      $client_name .= ' '.rtrim(chunk_split(rtrim($version, '0'), 1, '.'), '.');
      return $client_name;
    }
    else
      return '[unknown client]';
  }
  else
    return '[unknown format]';
}

/**
 * Returns the information hash for the specified structure.
 *
 * @param $struct
 *   The structure to generate the hash from
 * @return
 *    The SHA1 of the information section
 */
function info_hash($struct) {
  return sha1(bencode($struct['info']));
}

/**
 * Returns the passkey for the user.
 *
 * @param $user
 *   The user to generate the passkey from
 * @return
 *    The SHA1 of various information
 */
function make_passkey($user, $raw_output = false) {
  return sha1($user->uid . $user->name . time() . variable_get('drupal_private_key', ''), $raw_output);
}

/**
 * Set the torrent announce tier list.
 *
 * see: http://bittorrent.org/beps/bep_0012.html
 */
function torrent_set_announce_tier_list(&$torrent, $announce_tier_list/*, $primary_announce = null*/)
{
  if( count($announce_tier_list) == 0 )
  {
    unset($torrent['announce']);
    unset($torrent['announce-list']);
    return;
  }
  else
  {
    /*if( !isset($primary_announce) )
      $primary_announce = reset(reset($announce_list));*/

    $torrent['announce'] = reset(reset($announce_tier_list)); //$primary_announce;
    $torrent['announce-list'] = $announce_tier_list;
  }
}

/**
 * Get the torrent announce tier list.
 *
 * see: http://bittorrent.org/beps/bep_0012.html
 */
function torrent_get_announce_tier_list($torrent)
{
  if( isset($torrent['announce-list']) )
    return $torrent['announce-list'];
  else
    if( isset($torrent['announce']) )
      return array(array($torrent['announce']));
    else
      return array();
}

/**
 * Remove passkey from announce URL.
 */
function torrent_remove_passkey($announce_url)
{
  // Remove "#"-part if it exists. By the way, "#"-part in announce URL is an error.
  $fragment_pos = strpos($announce_url, '#');
  if ($fragment_pos !== FALSE) {
    $announce_url = substr($announce_url, 0, $fragment_pos);
  }
  
  $query_pos = strpos($announce_url, '?');
  if ($query_pos !== FALSE) {
    // Skip "?" sign
    $query_pos++;
    
    // Get current query string
    $query = substr($announce_url, $query_pos);
    
    // Compose new query string
    if (strlen($query) > 0) {
      parse_str($query, $params);
      unset($params['passkey']);
      $new_query = http_build_query($params);
    }
    else {
      $new_query = '';
    }

    // Replace
    if (strlen($new_query) > 0) {
      return substr_replace($announce_url, $new_query, $query_pos);
    }
    else {
      return substr_replace($announce_url, $new_query, $query_pos - 1);
    }   
  }
  else {
    return $announce_url; 
  }
}

/**
 * Remove passkeys from the torrent announce URLs.
 */
function torrent_remove_passkeys_from_announce_urls($torrent)
{
  if( isset($torrent['announce-list']) ) {
    foreach ($torrent['announce-list'] as &$announce_tier) {
      foreach ($announce_tier as &$announce_url) {
        $announce_url = torrent_remove_passkey($announce_url);
      }
    }
  }
    
  if( isset($torrent['announce']) ) {
    $torrent['announce'] = torrent_remove_passkey($torrent['announce']);
  }
    
  return $torrent;
}

/**
 * Calc torrent total size.
 *
 * see: http://wiki.theory.org/BitTorrentSpecification#Tracker_Response
 */
function torrent_calc_total_size($torrent)
{
  if( isset($torrent['info']['files']) )
  {
    $total_size = 0;
    foreach( $torrent['info']['files'] as $torrent_file )
      $total_size += $torrent_file['length'];
  }
  else
    $total_size = $torrent['info']['length'];

  return $total_size;
}

function torrent_compose_file_path_and_name($torrent_file_path) {
  if (is_array($torrent_file_path)) {
    $file_name = array_pop($torrent_file_path);
    $file_path = join('/', $torrent_file_path);
  }
  // This case is not documented at http://wiki.theory.org/BitTorrentSpecification#Info_Dictionary but already was implemented in the module.
  else {
    $pathinfo = pathinfo($torrent_file_path, PATHINFO_DIRNAME || PATHINFO_BASENAME);
    $file_path = $pathinfo['dirname'];
    $file_name = $pathinfo['basename'];
  }
  return array($file_path, $file_name);
}

/**
 * Pack peers struct (dictionary model) to string (binary model) // $peer['peer id'] values are ignored
 *
 * see: http://wiki.theory.org/BitTorrentSpecification#Tracker_Response
 */
function torrent_pack_peers($peer_list)
{
  $peers = '';

  foreach( $peer_list as $peer )
    $peers .= pack('N', ip2long($peer['ip'])).pack('n', $peer['port']);

  return $peers;
}

/**
 * Unpack peers string (binary model) to struct (dictionary model)
 *
 * see: http://wiki.theory.org/BitTorrentSpecification#Tracker_Response
 */
function torrent_unpack_peers($peers)
{
  $peer_list = array();

  $peer_count = floor(strlen($peers)/6);
  for( $i = 0; $i < $peer_count; $i++ )
  {
    $peer_data = substr($peers, $i*6, 6);

    $peer = array();
    $peer['ip'] = long2ip(reset(unpack('N', substr($peer_data, 0, 4))));
    $peer['port'] = reset(unpack('n', substr($peer_data, 4, 2)));
    $peer['peer id'] = null;

    $peer_list[] = $peer;
  }

  return $peer_list;
}

/**
 * Merges tracker response list into one
 */
function torrent_merge_tracker_response_list($tracker_response_list)
{
  $total_tracker_response = array('complete' => 0, 'incomplete' => 0, 'peers' => array());

  foreach( $tracker_response_list as $tracker_response )
  {
    $total_tracker_response['complete']   += $tracker_response['complete'];
    $total_tracker_response['incomplete'] += $tracker_response['incomplete'];

    if( !is_array($tracker_response['peers']) )
      $tracker_response['peers'] = torrent_unpack_peers($tracker_response['peers']);

    $total_tracker_response['peers'] = array_merge($total_tracker_response['peers'], $tracker_response['peers']);
  }

  return $total_tracker_response;
}

/**
 * Returns whether or not the specified structure is a valid torrent.
 *
 * @param $struct
 *   The structure that may or not be a torrent
 * @return TRUE if the specified structure meets minimum requirements to be a torrent
 */
function is_valid_torrent($struct) {
  if (is_array($struct)) {
    $base_required_keys = array(/*'announce', */'creation date', 'info');

    foreach($base_required_keys as $key) {
      if (!array_key_exists($key, $struct)) {
        return FALSE;
      }
    }

    $info_required_keys = array('pieces', 'piece length');
    foreach($info_required_keys as $key) {
      if (!array_key_exists($key, $struct['info'])) {
        return FALSE;
      }
    }

    if (array_key_exists('files', $struct['info'])) {
      // Multi-file torrent
      $file_required_keys = array('path', 'length');
      foreach($struct['info']['files'] as $file) {
        foreach($file_required_keys as $key) {
          if (!array_key_exists($key, $file)) {
            return FALSE;
          }
        }
      }
    }
    else {
      // Single file torrent
      $single_required_keys = array('name', 'length');
      foreach($single_required_keys as $key) {
        if (!array_key_exists($key, $struct['info'])) {
          return FALSE;
        }
      }
    }
  }
  else {
    return FALSE;
  }

  return TRUE;
}

/**
 * Returns a specified value within the HTTP response
 *
 * @param $msg
 *   The message to be returned within the HTTP response
 */
function bencode_response_raw($msg) {
  //ob_end_clean();
  header('Content-Type: text/plain');
  header('Pragma: no-cache');
  print($msg);
}
