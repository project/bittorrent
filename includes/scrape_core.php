<?php

/**
 * @file
 * Wrapper for scrape functionality.
 */

// Require the bt functions
require_once('bt_common.inc');

// Include bootstrap.inc and run the bootstrap
require_once('includes/bootstrap.inc');
drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);

// Include "module.inc"
require_once('includes/module.inc');

// Include settings functions
module_load_include('inc', 'settings', 'settings.api');

// Include "bt_tracker" module settings
module_load_include('inc', 'bt_tracker', 'bt_tracker.settings');

// Initializes $conf so we can use variable_get & settings_get
$conf = variable_init(isset($conf) ? $conf : array());

// Include "bt_tracker" "bt_tracker_scrape"
module_load_include('inc', 'bt_tracker', 'bt_tracker_scrape');

bt_tracker_scrape();
